# HanaHaato

This program will add a percentage text below the health bar in Guild Wars 2.

It takes a screenshot of the health bar every second and counts the amount of red pixles.

Other overlays may interfere with it.

Compiled version can be found here: [BitBucket](https://bitbucket.org/DarkDiamondV/gw2hp/raw/80182a910cdf3bbeb7e6ec2d23cef66305c53b89/Compiled/HanaHaato%20-%2024-04-16.zip) or [Google Drive](https://drive.google.com/open?id=0B0DdZhzgNL_0QU9RZkg4em5uVTA)

## Changes
### 24-04-16
1. Find health bar works again

### 23-04-16
1. Percentage less than 5% is shown as "<5%"
2. Combo box and HP text box is hidden when timer is inactive
3. Max HP combo box can be edited using haato.ini settings file
4. Monster for Spirit Vale will change automatically
5. Only numbers can be entered in max hp text box
6. Window is correctly minimized

### 06-03-16
1. Percentage is now shown with one decimal
2. Custom HP input works
3. Increased performance

## Installation

1. Extract the 7z file
2. Done!

## Usage

### Percentage

1. Start HanaHaato.exe
2. Target an enemy with full HP
3. Press "Find Health Bar"

### Timer

The timer will start automatically but has to be reset manually.

It will display the group DPS and time left with the same DPS.

The "Reset DPS Timer" button will only show when the timer is running.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D