#ifndef WINDOWMOVER_H
#define WINDOWMOVER_H

#include <QToolButton>

class WindowMover : public QToolButton
{
    Q_OBJECT
public:
    WindowMover(QWidget * parent = 0);
    ~WindowMover();

    void mousePressEvent(QMouseEvent* e);
    void mouseMoveEvent(QMouseEvent* e);
    int mouseX;
    int mouseY;

signals:
    void windowMoved(int, int);
};

#endif // WINDOWMOVER_H
