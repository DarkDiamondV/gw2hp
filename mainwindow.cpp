#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <windows.h>

#include <QWindow>
#include <QThread>
#include <QStyledItemDelegate>
#include <cmath>

static const int SPIRIT_VALE_MAPID = 1062;
static const int SALVATION_PASS_MAPID = 1149;
static const int MUMBLE_SLEEPTIME = 10000;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->layout()->setSizeConstraint(QLayout::SetFixedSize);
    this->setWindowFlags(this->windowFlags() | Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setAttribute(Qt::WA_ShowWithoutActivating);

    connect(ui->moveButton, SIGNAL(windowMoved(int,int)), this, SLOT(moveWindow(int,int)));
    connect(ui->minimizeButton, SIGNAL(clicked(bool)), this, SLOT(showMinimized()));
    connect(ui->closeButton, SIGNAL(clicked(bool)), this, SLOT(close()));
    ui->resetDpsTimerButton->hide();

    ui->maxHpTextBox->installEventFilter(this);
    ui->maxHpTextBox->setValidator(new QIntValidator(0, 999999999, this));

    QStyledItemDelegate* itemDelegate = new QStyledItemDelegate();
    ui->monsterComboBox->setItemDelegate(itemDelegate);
    connect(this, SIGNAL(destroyed(QObject*)), itemDelegate, SLOT(deleteLater()));

    // Create thread for the image grabber
    this->imageGrabberThread = new QThread();
    this->imageGrabber.moveToThread(this->imageGrabberThread);
    this->imageGrabberThread->start();

    connect(this->imageGrabberThread, SIGNAL(finished()), this->imageGrabberThread, SLOT(deleteLater()));
    connect(ui->findHealthBarButton, SIGNAL(clicked(bool)), &this->imageGrabber, SLOT(init()));
    connect(&this->imageGrabber, SIGNAL(healthBarFoundSignal(bool)), this, SLOT(setHealthBarStatus(bool)));
    connect(&this->imageGrabber, SIGNAL(newPercentage(double)), this, SLOT(setHealthPercentage(double)));

    // Create labels for HP
    this->labelWindow = new QWidget();
    this->labelWindow->setWindowFlags(this->labelWindow->windowFlags() | Qt::FramelessWindowHint | Qt::Drawer);
    this->labelWindow->setAttribute(Qt::WA_TranslucentBackground);

    this->labelLayout = new QVBoxLayout();
    this->labelLayout->setSpacing(0);
    this->labelLayout->setContentsMargins(0,0,0,0);

    this->percentageLabel = new OutlinedLabel();
    this->percentageLabel->setText("");
    this->percentageLabel->setContentsMargins(0,0,0,0);
    this->labelLayout->addWidget(this->percentageLabel);

    this->dpsLabel = new OutlinedLabel();
    this->dpsLabel->setText("");
    this->dpsLabel->setContentsMargins(0,0,0,0);
    this->labelLayout->addWidget(this->dpsLabel);

    this->timeLeftLabel = new OutlinedLabel();
    this->timeLeftLabel->setText("");
    this->timeLeftLabel->setContentsMargins(0,0,0,0);
    this->labelLayout->addWidget(this->timeLeftLabel);

    this->labelWindow->resize(this->labelLayout->minimumSize());

    this->labelWindow->setLayout(this->labelLayout);
    this->labelWindow->show();

    // Will make sure the main window stays on top
    // and that it does not get focus when clicked on.
    // Uses the Windows API because Qt's way just works sometimes.
    this->show();
    HWND mainHandle = (HWND) this->windowHandle()->winId();
    SetWindowPos(mainHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
    SetWindowLongPtr(mainHandle, GWL_EXSTYLE, GetWindowLongPtr(mainHandle, GWL_EXSTYLE) | WS_EX_NOACTIVATE | WS_EX_APPWINDOW);

    // Will make sure the window with all the labels stays on top
    // and that it is ignored when clicked on.
    // Uses the Windows API because Qt's way just works sometimes.
    HWND labelHandle = (HWND) this->labelWindow->windowHandle()->winId();
    SetWindowPos(labelHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW|SWP_NOSIZE|SWP_NOMOVE);
    SetWindowLongPtr(labelHandle, GWL_EXSTYLE, GetWindowLongPtr(labelHandle, GWL_EXSTYLE) | WS_EX_TRANSPARENT);

    this->timerActive = false;

    // Check settings file for previous values
    this->settings = new QSettings("haato.ini", QSettings::IniFormat);

    if(!this->settings->value("timer", true).toBool())
    {
        ui->timerCheckBox->setChecked(false);
    }

    if(this->imageGrabber.getHealthBarFound())
    {
        QMetaObject::invokeMethod(&this->imageGrabber, "run");
        this->setHealthBarStatus(true);
    }

    int windowX = this->settings->value("windowX", -1).toInt();
    int windowY = this->settings->value("windowY", -1).toInt();

    if(windowX != -1)
    {
        this->move(windowX, windowY);
    }

    this->settings->beginGroup("MaxHP");
    QStringList maxHpList = this->settings->allKeys();
    for(QString s : maxHpList)
    {
        ui->monsterComboBox->addItem(s, this->settings->value(s));
    }
    this->settings->endGroup();

    //Set up mumble and create thread for object
    this->mumble.setSleepTime(MUMBLE_SLEEPTIME);
    this->mumbleThread = new QThread();
    this->mumble.moveToThread(this->mumbleThread);
    this->mumbleThread->start();

    connect(&this->mumble, SIGNAL(positionChanged(float,float,float,float,float)), this, SLOT(characterMoved(float,float,float,float,float)));

    QMetaObject::invokeMethod(&this->mumble, "run");
}

MainWindow::~MainWindow()
{
    this->settings->setValue("windowX", this->x());
    this->settings->setValue("windowY", this->y());
    this->settings->sync();

    delete ui;
    this->settings->deleteLater();
    this->dpsLabel->deleteLater();
    this->percentageLabel->deleteLater();
    this->timeLeftLabel->deleteLater();
    this->labelLayout->deleteLater();
    this->labelWindow->deleteLater();

    this->imageGrabberThread->requestInterruption();
    this->imageGrabberThread->quit();
    this->imageGrabberThread->wait(10000);

    this->imageGrabberThread->deleteLater();

    this->mumbleThread->requestInterruption();
    this->mumbleThread->quit();
    this->mumbleThread->wait(10000);

    this->mumbleThread->deleteLater();
}

void MainWindow::moveWindow(int x, int y)
{
    this->move(x, y);
}

void MainWindow::on_findHealthBarButton_clicked()
{
    ui->findHealthBarButton->setText("Looking for Health Bar");
}

void MainWindow::setHealthBarStatus(bool status)
{
    if(status)
    {
        ui->findHealthBarButton->setText("Health Bar found");

        this->labelWindow->move(this->imageGrabber.getHealthBarX(), this->imageGrabber.getHealthBarY() + this->imageGrabber.getHealthBarHeight());
    }
    else
    {
        ui->findHealthBarButton->setText("Find Health Bar");
    }
}

void MainWindow::setHealthPercentage(double percent)
{
    if(percent > 0)
    {
        if(ui->timerCheckBox->isChecked() && percent < 100)
        {
            if(!this->timerActive)
            {
                this->timerActive = true;
                this->timer.restart();

                ui->resetDpsTimerButton->show();
            }

            int t = this->timer.elapsed() / 1000;

            // Because the world could explode if we don't do it
            if(t == 0)
            {
                t = 1;
            }

            int timeLeft = (t*percent) / (100 - percent);
            int minLeft = timeLeft / 60;
            int secLeft = timeLeft % 60;

            // Will create a string m:ss if m bigger than 0 or ss if m is 0
            QString timeString = (minLeft > 0 ? QString::number(minLeft) + ":" : "") +
                                 (secLeft < 10 ? "0" + QString::number(secLeft) : QString::number(secLeft));
            this->timeLeftLabel->setText(timeString);

            // Calculates the DPS using the formula: (maxHP - currentHP) / time
            this->dpsLabel->setText(QString::number((int)((this->maxHp - this->maxHp * (percent/100.0))/t)));
        }

        this->percentageLabel->setText(percent < 5 ? "<5%" : QString::number(round(percent)) + "%");
    }
    else
    {
        this->dpsLabel->setText("");
        this->percentageLabel->setText("");
        this->timeLeftLabel->setText("");
    }
}

void MainWindow::characterMoved(float x, float z, float y, float va0, float va1)
{
    switch(this->mumble.getMapid())
    {
    case SPIRIT_VALE_MAPID:
        if(y < -447)
        {
            ui->monsterComboBox->setCurrentText("Vale Guardian");
        }
        else if(y < -15)
        {
            ui->monsterComboBox->setCurrentText("Gorseval");
        }
        else
        {
            ui->monsterComboBox->setCurrentText("Sabetha");
        }
        break;
    case SALVATION_PASS_MAPID:
        // Check for Salvation Pass bosses
        break;
    }
}

void MainWindow::on_resetDpsTimerButton_clicked()
{
    this->timerActive = false;
    ui->resetDpsTimerButton->hide();

    this->dpsLabel->setText("0");
    this->timeLeftLabel->setText("Unknown");
}

void MainWindow::on_monsterComboBox_currentIndexChanged(int index)
{
    bool status;
    this->maxHp = ui->monsterComboBox->currentData().toInt(&status);
    if(!status)
    {
        this->maxHp = 1;
    }
}

void MainWindow::on_maxHpTextBox_textChanged(const QString &arg1)
{
    bool status;
    int hp = arg1.toInt(&status);

    if(!status || hp == 0)
    {
        on_monsterComboBox_currentIndexChanged(ui->monsterComboBox->currentIndex());
    }
    else
    {
        this->maxHp = hp;
    }
}

void MainWindow::on_timerCheckBox_toggled(bool checked)
{
    this->settings->setValue("timer", checked);

    if(!checked)
    {
        this->dpsLabel->setText("");
        this->timeLeftLabel->setText("");
        ui->monsterComboBox->hide();
        ui->maxHpTextBox->hide();
        ui->resetDpsTimerButton->hide();
    }
    else
    {
        ui->monsterComboBox->show();
        ui->maxHpTextBox->show();
        if(this->timerActive)
        {
            ui->resetDpsTimerButton->show();
        }
    }
}

bool MainWindow::eventFilter(QObject *obj, QEvent *ev)
{
    if(obj == ui->maxHpTextBox)
    {
        if(ev->type() == QEvent::MouseButtonPress)
        {
            this->activateWindow();
        }
    }
    return false;
}

void MainWindow::on_maxHpTextBox_editingFinished()
{
    ui->maxHpTextBox->clearFocus();
}
