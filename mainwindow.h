#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>
#include <QLayout>

#include "imagegrabber.h"
#include "outlinedlabel.h"
#include "mumble.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void moveWindow(int x, int y);

private slots:
    void on_findHealthBarButton_clicked();
    void setHealthBarStatus(bool status);
    void setHealthPercentage(double percent);
    void characterMoved(float x, float z, float y, float va0, float va1);


    void on_resetDpsTimerButton_clicked();
    void on_monsterComboBox_currentIndexChanged(int index);
    void on_maxHpTextBox_textChanged(const QString &arg1);
    void on_timerCheckBox_toggled(bool checked);
    void on_maxHpTextBox_editingFinished();

protected:
    bool eventFilter(QObject *obj, QEvent *ev);

private:
    QSettings* settings;

    Ui::MainWindow *ui;
    ImageGrabber imageGrabber;
    QThread* imageGrabberThread;

    Mumble mumble;
    QThread* mumbleThread;

    OutlinedLabel* percentageLabel;
    OutlinedLabel* dpsLabel;
    OutlinedLabel* timeLeftLabel;

    QWidget* labelWindow;
    QVBoxLayout* labelLayout;

    QTime timer;
    bool timerActive;

    int maxHp;
};

#endif // MAINWINDOW_H
