#ifndef OUTLINEDLABEL_H
#define OUTLINEDLABEL_H

#include <QLabel>

class OutlinedLabel : public QLabel
{
    Q_OBJECT

public:
    OutlinedLabel(QWidget * parent = 0);
    ~OutlinedLabel();

protected slots:
    virtual void paintEvent(QPaintEvent* event);
};

#endif // OUTLINEDLABEL_H
