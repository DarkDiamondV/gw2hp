#include "windowmover.h"

#include <QMouseEvent>

WindowMover::WindowMover(QWidget *parent)
{
}

WindowMover::~WindowMover()
{

}


void WindowMover::mousePressEvent(QMouseEvent *e)
{
    this->mouseX = e->x();
    this->mouseY = e->y();
}

void WindowMover::mouseMoveEvent(QMouseEvent *e)
{
    emit windowMoved(e->globalX() - this->mouseX - 10, e->globalY() - this->mouseY - 10);
}
