#-------------------------------------------------
#
# Project created by QtCreator 2016-02-09T21:02:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HanaHaato
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    imagegrabber.cpp \
    outlinedlabel.cpp \
    windowmover.cpp \
    mumble.cpp

HEADERS  += mainwindow.h \
    imagegrabber.h \
    outlinedlabel.h \
    windowmover.h \
    mumble.h

FORMS    += mainwindow.ui

RC_ICONS += haato.ico

CONFIG += c++11
