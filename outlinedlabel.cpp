#include "outlinedlabel.h"

#include <QPainter>
#include <QPaintEvent>
#include <QFont>
#include <QRect>

OutlinedLabel::OutlinedLabel(QWidget *parent)
{

}

OutlinedLabel::~OutlinedLabel()
{

}

void OutlinedLabel::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QPainterPath path;
    QPen pen;

    pen.setWidth(3);
    pen.setColor(Qt::black);
    painter.setFont(this->font());
    painter.setPen(pen);

    path.addText(0, this->height()-1, this->font(), this->text());

    painter.drawPath(path);
    painter.setPen(QPen(Qt::white));
    painter.drawText(event->rect(), text());
}
