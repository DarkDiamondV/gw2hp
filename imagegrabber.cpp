#include "imagegrabber.h"

#include <QGuiApplication>
#include <QTime>
#include <QRect>
#include <QImage>
#include <QPixmap>
#include <QScreen>
#include <QThread>

#include <windows.h>

static const int RGB_RED = 2;
static const int RGB_GREEN = 1;
static const int RGB_BLUE = 0;

static const int RED_LOWER = 100;
static const int RED_UPPER = 165;
static const int GREEN_LOWER = 20;
static const int GREEN_UPPER = 40;
static const int BLUE_LOWER = 10;
static const int BLUE_UPPER = 25;

static const int MIN_HEALTH_BAR_SIZE = 200;

ImageGrabber::ImageGrabber()
{
    this->screen = QGuiApplication::primaryScreen();
    this->screenSize = this->screen->geometry();

    this->settings = new QSettings("haato.ini", QSettings::IniFormat);

    this->healthBarHeight = this->settings->value("healthBarHeight", 0).toInt();
    this->healthBarWidth = this->settings->value("healthBarWidth", 0).toInt();
    this->healthBarX = this->settings->value("healthBarX", 0).toInt();
    this->healthBarY = this->settings->value("healthBarY", 0).toInt();

    this->healthBarFound = false;

    if(this->healthBarHeight != 0)
    {
        this->healthBarArea = this->healthBarWidth * this->healthBarHeight;
        this->healthBarFound = true;

        emit this->healthBarFoundSignal(true);
    }
}

ImageGrabber::~ImageGrabber()
{
    delete this->settings;
}

int ImageGrabber::getHealthBarWidth() const
{
    return this->healthBarWidth;
}

int ImageGrabber::getHealthBarHeight() const
{
    return this->healthBarHeight;
}

int ImageGrabber::getHealthBarX() const
{
    return this->healthBarX;
}

int ImageGrabber::getHealthBarY() const
{
    return this->healthBarY;
}

bool ImageGrabber::getHealthBarFound() const
{
    return this->healthBarFound;
}

void ImageGrabber::init()
{
    int imgX = this->screenSize.width() * 0.38;
    int imgY = this->screenSize.height() * 0.05;

    // Ought to be enough for all UI sizes
    QImage img = screen->grabWindow(0, imgX, imgY, this->screenSize.width() * 0.24, this->screenSize.height() * 0.10).toImage();

    this->healthBarWidth = 0;
    this->healthBarHeight = 0;

    this->healthBarX = 0;
    this->healthBarY = 0;

    this->healthBarFound = false;

    // Will look for health bar and find both position and size
    for(this->healthBarY = 0; this->healthBarY < img.height() && !this->healthBarFound; this->healthBarY++)
    {
        for(this->healthBarX = 0; this->healthBarX < img.width() && !this->healthBarFound; this->healthBarX++)
        {
            QRgb pixel = img.pixel(this->healthBarX, this->healthBarY);
            unsigned char* pixelBytes = (unsigned char*) &pixel;

            if(pixelBytes[RGB_RED] > RED_LOWER && pixelBytes[RGB_RED] < RED_UPPER &&
               pixelBytes[RGB_GREEN] > GREEN_LOWER && pixelBytes[RGB_GREEN] < GREEN_UPPER &&
               pixelBytes[RGB_BLUE] > BLUE_LOWER && pixelBytes[RGB_BLUE] < BLUE_UPPER)
            {
                this->healthBarWidth++;
            }
            else
            {
                if(this->healthBarWidth > MIN_HEALTH_BAR_SIZE)
                {
                    this->healthBarFound = true;

                    this->healthBarX = this->healthBarX - this->healthBarWidth;

                    do
                    {
                        this->healthBarY++;
                        healthBarHeight++;
                        pixel = img.pixel(this->healthBarX, this->healthBarY);
                        pixelBytes = (unsigned char*) &pixel;

                    }while(pixelBytes[RGB_RED] > RED_LOWER && pixelBytes[RGB_RED] < RED_UPPER &&
                           pixelBytes[RGB_GREEN] > GREEN_LOWER && pixelBytes[RGB_GREEN] < GREEN_UPPER &&
                           pixelBytes[RGB_BLUE] > BLUE_LOWER && pixelBytes[RGB_BLUE] < BLUE_UPPER);

                    this->healthBarY = this->healthBarY - healthBarHeight;
                }
                else
                {
                    this->healthBarWidth = 0;
                }
            }
        }
    }

    if(!healthBarFound)
    {
        emit this->healthBarFoundSignal(false);

        return;
    }

    // Corrects position to screen coordinates
    this->healthBarX = imgX + this->healthBarX - 1;
    this->healthBarY = imgY + this->healthBarY - 1;

    this->healthBarArea = this->healthBarHeight * this->healthBarWidth;

    this->settings->setValue("healthBarHeight", this->healthBarHeight);
    this->settings->setValue("healthBarWidth", this->healthBarWidth);
    this->settings->setValue("healthBarX", this->healthBarX);
    this->settings->setValue("healthBarY", this->healthBarY);

    emit this->healthBarFoundSignal(true);

    QMetaObject::invokeMethod(this, "run");

    return;
}

void ImageGrabber::run()
{
    if(!this->healthBarFound)
    {
        return;
    }

    while(!this->thread()->isInterruptionRequested())
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents);
        this->thread()->sleep(1);

        QImage img = this->screen->grabWindow(0, this->healthBarX, this->healthBarY, this->healthBarWidth, this->healthBarHeight).toImage();

        int redPixel = 0;
        int nonRedPixel = 0;

        for(int y = 0; y < this->healthBarHeight; y++)
        {
            for(int x = 0; x < this->healthBarWidth; x++)
            {
                QRgb pixel = img.pixel(x, y);
                unsigned char* pixelBytes = (unsigned char*) &pixel;

                if(pixelBytes[RGB_RED] > RED_LOWER && pixelBytes[RGB_RED] < RED_UPPER &&
                   pixelBytes[RGB_GREEN] > GREEN_LOWER && pixelBytes[RGB_GREEN] < GREEN_UPPER &&
                   pixelBytes[RGB_BLUE] > BLUE_LOWER && pixelBytes[RGB_BLUE] < BLUE_UPPER)
                {
                    redPixel++;
                }
                else
                {
                    nonRedPixel++;

                    if(nonRedPixel > 10)
                    {
                        nonRedPixel = 0;
                        break;
                    }
                }
            }
        }

        emit newPercentage(redPixel*100.0 / this->healthBarArea);
    }
}

