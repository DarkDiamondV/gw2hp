#ifndef IMAGEGRABBER_H
#define IMAGEGRABBER_H

#include <QObject>
#include <QRect>
#include <QScreen>
#include <QSettings>

class ImageGrabber : public QObject {
    Q_OBJECT
public:
    ImageGrabber();
    ~ImageGrabber();
    QScreen* screen;
    QRect screenSize;
    int getHealthBarWidth() const;
    int getHealthBarHeight() const;
    int getHealthBarX() const;
    int getHealthBarY() const;
    bool getHealthBarFound() const;

private:
    QSettings* settings;
    int healthBarWidth;
    int healthBarHeight;
    int healthBarX;
    int healthBarY;
    int healthBarArea;
    bool healthBarFound;

public slots:
    void run();
    void init();

signals:
    void healthBarFoundSignal(bool);
    void newPercentage(double);

};

#endif // IMAGEGRABBER_H
